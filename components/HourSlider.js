import React, { Component } from 'react';
import {View, Text, StyleSheet, AsyncStorage} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import styles from '../styles';

const listHours = [...Array(24).keys()] //Liste des heures (de 0 à 23)
const listMinutes = [...Array(60).keys()] //Liste des minutes (de 0 à 59)
const numberFontSize = 140; //Taille de la police des chiffres

/**
 * Carousel de chiffre pour la selection de l'heure.
 * Utilisation de la librairie Carousel pour gérer le scroll infini l'animation de scroll.
 * TODO: Gérer le bug d'actualisation lorsqu'on arrive au bout du carousel. Deuxième option, changer de librairie.
 */
export class HourSlider extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hour: '',
            minutes: ''
        }
    }

    /**
     * Sauvegarde locale de l'heure selectionnée.
     */
    async stockeHeure() {
        try {
            let date = new Date();
            date.setHours(this.state.hour, this.state.minutes, 0);
            // console.log('ALARM HOUR', date.toTimeString())
            await AsyncStorage.setItem('alarm', date.toTimeString());
            AsyncStorage.getItem('alarm').then((data) => {
                console.log({data});
            });
        } catch (error) {
            console.log(error);
        }
    };

    //Avant que le composant se monte, on récupère l'heure sauvegardé. S'il n'y en a pas, on initialise à 00:00.
    componentWillMount() {
        AsyncStorage.getItem('alarm').then((data) => {
            console.log({data});
            if(data == undefined) {
                let date = new Date();
                date.setHours('00', '00', 0);
            }
        })
    }

    render() {
        return (
            <View style={viewStyle.container}>
                <View>
                    <Carousel
                    data={listHours}
                    disableIntervalMomentum={false}
                    bounces={true}
                    loop
                    vertical
                    itemHeight={numberFontSize}
                    sliderHeight={numberFontSize*3}
                    enableMomentum
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={0.6}
                    useScrollView={false}
                    onSnapToItem={(el) => {
                        this.setState({hour: el}, 
                        () => {
                            this.stockeHeure();
                        });
                    }}
                    renderItem={({ item }) => (
                        <Text style={[textStyle.number, {textAlign: "right"}]}>{item < 10 ? 0 + '' + item : item}</Text>
                    )}
                    />
                </View>
                <Text style={viewStyle.doublePoint}>:</Text>
                <View>
                    <Carousel
                    data={listMinutes}
                    disableIntervalMomentum={false}
                    bounces={true}
                    loop
                    vertical
                    itemHeight={numberFontSize}
                    sliderHeight={numberFontSize*3}
                    enableMomentum
                    inactiveSlideScale={1}
                    inactiveSlideOpacity={0.6}
                    useScrollView={false}
                    onSnapToItem={(el) => {
                        this.setState({minutes: el}, 
                            () => {
                                this.stockeHeure();
                            });
                    }}
                    renderItem={({ item }) => (
                        <Text style={[textStyle.number, {textAlign: "left"}]}>{item < 10 ? 0 + '' + item : item}</Text>
                    )}
                    />
                </View>
            </View>
        );
    }
}

const viewStyle = StyleSheet.create({
    container: {
        display: "flex",
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    
    doublePoint: {
        height: '100%',
        fontSize: 100,
        color: styles.secondary_color,
        textAlignVertical: "center", 
        marginTop: 20, 
        textAlign: "center"
    }
})

const textStyle = StyleSheet.create({
    number: {
        textAlignVertical: 'bottom',
        color: '#FFF', 
        fontSize: numberFontSize, 
        fontFamily: 'Roboto Condensed', 
        includeFontPadding: false
    }
});