import React, { Component } from 'react';
import styled from 'styled-components/native';
import { Animated, StyleSheet, View, AsyncStorage } from 'react-native';
import styles from '../styles';
import { LinearGradient } from 'expo-linear-gradient';

const StyledContainer = styled.TouchableOpacity`
    height: 30px;
    width: 60px;
    background-color: ${styles.primary_color};
    border-radius: 100;
    display: flex;
`

const StyledSlider = StyleSheet.create( {
    slider: {
        height: 26,
        width: 26,
        borderRadius: 100,
        position: 'absolute',
        top: 2
    }});

/**
 * Bouton toggle pour la gestion d'état on/off
 */
export class ButtonSlider extends Component {

    state = {
        isOn: false,
        animatedValue: new Animated.Value(2),
    }

    /**
     * Mise a jour de l'état et animation de slide.
     */
    async toggleHandle() {
        await this.setState(
            { isOn: !this.state.isOn },
            () => {
            Animated.timing(
                this.state.animatedValue,
                { toValue: this.state.isOn ? 30 : 2 }
            ).start()
            }
        )
        
        await this.props.callback(JSON.stringify(this.state.isOn));
    }

    /**
     * Récupération de l'état sauvegardé.
     */
    componentDidMount() {
        AsyncStorage.getItem('active').then((data) => {
            this.setState({isOn: JSON.parse(data) }, 
                () => {
                    Animated.timing(
                        this.state.animatedValue,
                        { toValue: this.state.isOn ? 30 : 2 }
                    ).start()
                });
        })
    }

    render() {
        return (
        <LinearGradient colors={['#FDD5F4', '#FFE6C7']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} style={{padding: 2, borderRadius: 100}}>
            <StyledContainer activeOpacity={0.8} onPress={() => this.toggleHandle()} style={{backgroundColor: this.state.isOn ? "transparent" : styles.primary_color }}>
                <Animated.View style={[StyledSlider.slider, {left: this.state.animatedValue, backgroundColor: this.state.isOn ? styles.primary_color : '#FDD5F4'}]}>
                </Animated.View>
            </StyledContainer>
        </LinearGradient>);
    }
}