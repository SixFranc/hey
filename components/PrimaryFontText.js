import React, { Component } from 'react';
import styled from 'styled-components/native';
import { Text } from 'react-native';

const StyledText = styled.Text`
    font-family: 'Roboto black'
`

export const PrimaryFontText = ({style, children}) => {
return(<StyledText style={style}>{children}</StyledText>)
}