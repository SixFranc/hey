import React, { Component } from 'react';
import styled from 'styled-components/native';
import { Animated, StyleSheet, View, Text } from 'react-native';
import styles from '../styles';
import { LinearGradient } from 'expo-linear-gradient';

//Déclaration des style de chaque elément du composant
const StyledContainer = styled.TouchableOpacity`
    height: 60px;
    width: 100%;
    display: flex;
    justify-content: center;
    position: relative;
`

const StyledText = styled.Text`
    text-align: center;
    color: ${styles.primary_color};
    width: 100%;
    font-size: 16;
`

const StyledLinearGradient = {
    position: 'absolute',
    width: '100%', 
    height: '100%', 
    borderRadius: 5,
}

/**
 * Bouton Call to Action textuel
 */
export class CTA extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let { text, onPress, actif } = this.props;
        
        //Petit hack si la props est undefined. 
        //TODO: Améliorer le comportement
        if(actif === undefined) {
            actif = true;
        }

        return (
            <StyledContainer onPress={onPress} activeOpacity={1} style={{zIndex: 10, opacity:actif ? 1 : 0.5, backgroundColor: "transparent", borderRadius: 10, borderWidth: actif ? 0 : 3, borderColor: actif ? styles.primary_color : '#FDD5F4'}}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={ actif ? ['#FDD5F4', '#FFE6C7'] : [styles.primary_color, styles.primary_color]} style={[StyledLinearGradient, { zIndex: 1}]}></LinearGradient>
                <StyledText style={{zIndex : 2, color: actif ? styles.primary_color : '#FDD5F4'}}>{text}</StyledText>
            </StyledContainer>
        )
    }
}