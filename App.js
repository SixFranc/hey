
import React, { Component } from 'react';
import { StyleSheet, Text, View, AsyncStorage, AppState, DeviceEventEmitter } from 'react-native';
import styles from './styles.js';
import { AppRouter } from './app/routing';
import * as Font from 'expo-font';
import BackgroundJob from 'react-native-background-job';
import { Actions } from 'react-native-router-flux';
import { AppConstantes } from './app/app.constants.js';

import invokeApp from 'react-native-invoke-app';
const yourObject = { route: AppConstantes.ROUTES.alarmRinging };

/**
 * Déclaration de la tâche de fond qui test l'heure pour savoir si le réveil doit sonner.
 */
const backgroundJob = {
  jobKey: "myJob",
  job: async () => {
    const keys = await AsyncStorage.getAllKeys()
    const items = await AsyncStorage.multiGet(keys)

    
    let dateActuelle = new Date();
    let date = new Date();
    date.setHours(dateActuelle.getHours(), dateActuelle.getMinutes(), 0);
    date = date.toTimeString();
    let dateRegistered;
    AsyncStorage.getItem('alarm').then((data) => {
      dateRegistered = data;
      console.log({date, dateRegistered});
      if( date === dateRegistered) {
        console.log('Meme heure !');
        invokeApp({
          data: yourObject,
        });
      }
    });
  }
};
  
/**
 * Lancement de la tâche de fond
 */
BackgroundJob.register(backgroundJob);
  
var backgroundSchedule = {
  jobKey: "myJob",
  period: 5000,
  exact: true,
  allowExecutionInForeground: true
}
  
/**
 * Lancement de la tâche de fond avec les paramètre d'éxecution : période, tâche de fond...
 */
BackgroundJob.schedule(backgroundSchedule)
  .then(() => console.log("Success"))
  .catch(err => console.err(err));

console.disableYellowBox = true;
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  /**
   * Déclaration de l'évènement qui se déclenche quand l'application est lancé depuis le background
   */
  componentWillMount() {
    DeviceEventEmitter.addListener('appInvoked', (data) => {
      const { route } = data;
      console.log({route});
      console.log('LANCEMENT DE L\'APP')
      
      // Using react-navigation library for navigation.
      Actions[route]();
    });
  }

  /**
   * Déclaration de l'évènement qui se déclanche quand l'application change de statut
   */
  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
  }

  //TODO: ENEVER QUAND LA FONCTIONNALITé SERA FINIE
  _handleAppStateChange = nextAppState => {
    if (nextAppState === 'inative' || nextAppState === 'background' ) {
      
    } else {
      BackgroundJob.schedule(backgroundSchedule)
      .then(() => console.log("Success"))
      .catch(err => console.err(err));
    }
  };

  /**
   * Chargement des fonts avant d'afficher l'application
   */
  async componentWillMount() {
    await Font.loadAsync({
      "Roboto black": require("./assets/fonts/Roboto-Thin.ttf"),
      "Roboto Regular": require("./assets/fonts/Roboto-Regular.ttf"),
      "Roboto Condensed": require("./assets/fonts/RobotoCondensed-Light.ttf"),
      "Roboto Light": require('./assets/fonts/Roboto-Light.ttf'),
      "Roboto Thin": require('./assets/fonts/Roboto-Thin.ttf'),
      "Typewriter": require('./assets/fonts/Tox-Typewriter.ttf')
    });
    this.setState({ loading: false });
  }
  
  /**
   * Si l'application a fini de charger les fonts on l'affiche sinon on affiche une page de chargement.
   */
  render() {
    if(!this.state.loading) {
      return (
          <View style={vueStyle.vue}>
            <AppRouter></AppRouter> 
          </View>
      );
    } else {
      return <View>
        <Text>Loading</Text>
      </View>
    }
  }
}

const vueStyle = StyleSheet.create({
  vue: {
    backgroundColor: styles.primary_color, 
    flex: 1,
    color: styles.secondary_color 
  }
})
