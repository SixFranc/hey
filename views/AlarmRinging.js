import React, { Component } from 'react';
import { ScrollView, Text, View, StyleSheet, Image, Animated, Dimensions,
    PanResponder,
    Easing,
    AsyncStorage} from 'react-native';
import styles from '../styles';
import { LinearGradient } from 'expo-linear-gradient';
import { messagesDB } from '../firebase';
import Sound from 'react-native-sound';
import FlecheHaut from '../assets/flecheHaut.svg';
import FlecheMilieu from '../assets/flecheMilieu.svg';
import FlecheBas from '../assets/flecheBas.svg';
const deviceHeight = Dimensions.get('window').height;

/**
 * Vue de l'alarme qui sonne 
 */
export default class AlarmRinging extends Component {

    constructor(props) {
        super(props);
        this.dbMessage = '';
        this.soundPlayed = undefined;
        
        this.state = { 
            //States utile à l'animation du scroll
            scroll: new Animated.Value(deviceHeight), 
            scrollUpTemp: deviceHeight, 
            scrollDownTemp: new Animated.Value(250), 
            scrollEnd : false, 
            scrollInactive: false,

            //States du contenu du message reçu
            messageContent: '',
            tagline: '',
            location: '',

            //heure du réveil
            hour: ''
        }
    }

    //Déclaration du Pan pour gérer les évènement et ses déclencheur. 
    //Indispensable pour ne garder que le scroll vertical ascendant.
    panResponder = PanResponder.create({
        onMoveShouldSetPanResponderCapture: () => true,
        onPanResponderStart: (_, gestureState) => this.onMoveStart(),
        onPanResponderMove: (_, gestureState) => this.onMove(gestureState),
        onPanResponderRelease: (_, gestureState) => this.onMoveEnd(gestureState),
    });

    onMoveStart() {
        
    }
    
    /**
     * Lorsque que le scroll est en cours ou met à jour l'état de scroll vers le haut.
     * @param {*} gestureState 
     */
    onMove(gestureState) {
        this.setState({scrollEnd: false})
        if(gestureState.dy <= 0) {
            this.setState({scrollUpTemp: deviceHeight + gestureState.dy});
        } 
    }

    /**
     * Lorsque que le scroll se termine, si il dépasse une hauteur définie alors on affiche le message reçu en déclanchant l'animation. 
     * Sinon on remet l'état de scroll à son état initial.
     * @param {*} gestureState 
     */
    onMoveEnd(gestureState) {
        if(this.state.scrollInactive === false) {
            if(gestureState.dy <= 0) {
                if(gestureState.dy < -120) {

                    if(this.soundPlayed != undefined) {
                        this.soundPlayed.release();
                    }

                    this.setState({scrollInactive: true});
                    this.state.scroll.setValue(this.state.scrollUpTemp);
                    this.setState( () => {
                        Animated.timing(
                            this.state.scroll,
                            { toValue: 0,
                            easing: Easing.inOut(Easing.cubic)
                        }
                        ).start(() => this.displayText())}
                    );
                } else {
                    this.setState({scrollEnd: true});
                    this.state.scrollDownTemp.setValue(this.state.scrollUpTemp);
                    this.setState( 
                        {scrollUpTemp: deviceHeight},
                        () => {
                        Animated.timing(
                            this.state.scrollDownTemp,
                            { toValue: deviceHeight }
                        ).start()}
                    );
                }
            }
        }
    }

    /**
     * Animation d'affichage du texte lettre par lettre.
     */
    displayText() {
        let textLetterArray = this.dbMessage.split("");
        let i = 0;
        let interval = setInterval(() => 
        {
            const oldContent =  this.state.messageContent;
            this.setState({messageContent: oldContent + textLetterArray[i]});
            ++i;
            if(i === textLetterArray.length) {
                console.log('STOP');
                clearInterval(interval)
            }
        }, 20)
    }

    /**
     * Quand le composant a fini de se monter, on charge la sonnerie enregistrée et le message reçu.
     */
    async componentDidMount() {
        try {
            AsyncStorage.getItem('alarm').then((data) => {
                let dateSplit = data.split(':');
                let hour = dateSplit[0] + ':' + dateSplit[1];
                this.setState({hour})
                
            })
            AsyncStorage.getItem('ring').then((data) => {
                let sound = JSON.parse(data);
                this.soundPlayed = new Sound(sound.uri, null, (error) => {
                    if (error) {
                        console.log(error)
                    }
                    //Le son est joué quand il est chargé et si il n'y a pas d'erreur
                    this.soundPlayed.play();
                });
            });
            let res = await messagesDB.getRandomMessage();
            //Temporaire, utile à la démo.
            this.dbMessage = 'Nous vous souhaitons une bonne santé et de longues années de vie et une bonne navigation.'//res.message;
            this.setState({
                location: res.location,
                tagline: res.tagline
            })
        } catch(e) {
            console.log(e);
        }
    }

    render() {
        return (
            <View style={{flex: 1}}>
            <View  
            style={[viewStyle.container]}
            {...this.panResponder.panHandlers}>
                    <View style={[viewStyle.view, viewStyle.alarm]}>
                        <Text style={[viewStyle.textColor, viewStyle.hey]}>Hey François !</Text>
                        <Text style={[viewStyle.textColor, viewStyle.hour]}>{this.state.hour}</Text>
                        <Text style={[viewStyle.textColor, viewStyle.day]}>30 avril 2020</Text>
                        <View style={{position: 'absolute', bottom: 10, display: 'flex',  alignItems: 'center', width: '100%'}}>
                            <FlecheHaut></FlecheHaut>
                            <FlecheMilieu></FlecheMilieu>
                            <FlecheBas></FlecheBas>
                            <Text style={{opacity: 0.3, textAlign: 'center', fontFamily: 'Roboto Light', fontSize: 18, letterSpacing: 2, color: '#FFC9FF'}}>Mon message du jour</Text>
                        </View>
                    </View>
                    <Animated.View style={{ width: '100%', flex: 1, position: 'absolute', top: this.state.scrollInactive ? this.state.scroll : (this.state.scrollEnd ? this.state.scrollDownTemp : this.state.scrollUpTemp), height: '100%'}}>
                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={['#FDD5F4', '#FFE6C7']} style={[viewStyle.view, viewStyle.gradient]}>
                            <View style={viewStyle.textAreaContainer}>
                                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ flex: 1 }}>
                                    <View style={{height: '100%'}}>
                                        <Text style={viewStyle.message}>
                                            {this.state.messageContent}
                                        </Text>
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={viewStyle.taglineContainer}>
                                <Text style={[ viewStyle.tagline, { fontSize: 18 }]}>- Anatolii</Text>
                                <Text style={[ viewStyle.tagline, { fontSize: 14 }]}>Moscou, Russie</Text>
                            </View>
                        </LinearGradient>
                    </Animated.View>
            </View>
            </View>)
    }
}

const viewStyle = StyleSheet.create({
    container: {
        backgroundColor: styles.primary_color,
        height: '100%'
    },  
    scrollContainer: {
    },
    view: {
        height: '100%',
    },
    textColor: {
        color: styles.secondary_color,
    },
    alarm: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
    },
    hey: {
        fontFamily: 'Roboto Light',
        fontSize: 52,
        textAlign: 'center'
    },
    hour: {
        fontSize: 130,
        textAlign: 'center',
        fontFamily: 'Roboto Condensed', 
        includeFontPadding: false
    },
    day: {
        fontSize: 24,
        textAlign: 'center',
        fontFamily: 'Roboto Light'
    },
    gradient: {
        width: '100%',
        padding: 50
    }, 
    message: { 
        fontFamily: "Typewriter", 
        fontSize: 20, 
        letterSpacing: 4,
    }, 
    textAreaContainer: {
        flex: 9,
        paddingRight: 50,
        width: '100%'
    }, 
    textArea: {
        opacity: 1, 
        textAlignVertical: 'center', 
        flex: 1, 
        fontFamily: "Typewriter", 
        fontSize: 18, 
        letterSpacing: 4
    },
    tagline: {
        textAlign: 'right', 
        fontFamily: "Typewriter", 
        letterSpacing: 3,
        color: styles.primary_color
    }, 
    taglineContainer: { 
        flex:1, 
        display: 'flex', 
        justifyContent: 'flex-end'
    }
})