import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, AsyncStorage } from 'react-native';
import styles from '../styles.js';


import { CTA } from '../components/CTA';
import { HourSlider } from '../components/HourSlider';
import { ButtonSlider } from '../components/ButtonSlider';
import { Actions } from 'react-native-router-flux';
import { AppConstantes } from '../app/app.constants.js';

import RingtoneManager from 'react-native-ringtone-manager';


/**
 * Vue pour la programmation du réveil.
 */
export default class AlarmView extends Component {
    constructor(props) {
        super(props);
        this.state = { hour: '', minutes: '', ringName: ''};
    }

    /**
     * Redirection vers la liste des sonneries.
     */
    goToRingList() {
        Actions[AppConstantes.ROUTES.ringList]();
    }

    /**
     * Redirection vers l'envoi de message.
     */
    goToMessageSending(rep = false) {
        Actions[AppConstantes.ROUTES.messageSending]();
    }

    /**
     * Sauvegarde de l'état actif/inactif du réveil
     * @param {} active 
     */
    async saveActiveState(active) {
        try {
            await AsyncStorage.setItem('active', active);
        } catch (error) {
            console.log(error);
        }
    }

    /**
     * Au montage de la vue on charge la sonnerie enregistré pour l'afficher.
     */
    async componentWillMount() {
        try {
            let ring = await AsyncStorage.getItem('ring');
            console.log(ring); 
            if(ring != undefined) {
                this.setState({ringName: JSON.parse(ring).title});
            } else {
                RingtoneManager.getRingtones((data) => { 
                    this.setState({ ringName: data[0].title});
                });
            }

        } catch(e) {
            console.log(e);
        }
    }

    render() {
        return (
            <View style={vueStyle.vue}>
                <View style={{flex: 5}}>
                    <HourSlider></HourSlider>
                </View>
                <View style={inputStyle.container}>
                    <View style={inputStyle.input}>
                        <Text style={inputStyle.label}>Sonnerie</Text>
                        <TouchableOpacity activeOpacity={1} onPress={this.goToRingList}>
                            <Text style={inputStyle.inputRing}>{this.state.ringName} ></Text>
                        </TouchableOpacity>
                    </View>
                    <View style={inputStyle.input}>
                        <Text style={inputStyle.label}>Actif</Text>
                        <ButtonSlider callback={this.saveActiveState}></ButtonSlider>
                    </View>
                </View>
                <View>
                    <CTA text={"Rédiger un message"} onPress={() => this.goToMessageSending()}></CTA>
                </View>
            </View>
        );
    }
}

const vueStyle = StyleSheet.create({
    vue: {
        backgroundColor: styles.primary_color, 
        flex: 1, 
        padding: 50
    }
})

const inputStyle = StyleSheet.create({
    container: {
        flex: 1, 
        display: 'flex',
        justifyContent: 'space-between',
        marginVertical: 50
    },
    input: {
        display: 'flex', 
        flexDirection: 'row',
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    label: {
        color: '#FFF', 
        fontSize: 18, 
        fontFamily: 'Roboto Light'
    },
    inputRing: {
        color: '#FFF', 
        fontSize: 16, 
        fontFamily: 'Roboto Thin'
    }
})
