import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import styles from '../styles';
import {CTA} from '../components/CTA'
import { Actions } from 'react-native-router-flux';
import { AppConstantes } from '../app/app.constants';
import { messagesDB } from '../firebase';

import FlecheRetour from '../assets/flecheRetour.svg';

/**
 * Vue pour l'envoi de message
 */
export default class MessageSending extends Component {
    constructor(props) {
        super(props);
        this.state = { message : '', tagline: '', location: '', isEmpty: true};
    }

    /**
     * Redirection vers la programmation du réveil.
     */
    returnToAlarmView(withSave) {
        Actions[AppConstantes.ROUTES.alarm]();
    }

    /**
     * Construction de l'objet message et envoie à L'API Firebase pour l'enregistrement.
     */
    async sendMessage() {
        //Envoie du message à Firebase
        //Envoie vers une page de validation de l'envoi
        try {
            let messageSent = await messagesDB.saveMessage({
                    message: this.state.message,
                    tagline: 'Francis',
                    location: 'Paris, France'
                });
            
            //Temporaire pour la démo.
            //Normalement ici on devrait retourner sur la vue de programmation de réveil. 
            //L'affichage de la vue du réveil qui sonne ne devrait se faire seulement lorsque l'heure programmé est l'heure actuelle.
            //Cependant le lancement d'une application vie une tâche de fond ne peut apparement se faire seulement en creusant et implémentant la fonctionnalité dans le code android.
            Actions[AppConstantes.ROUTES.alarmRinging]();
        } catch(e) {
            console.log(e);
        }
    }

    render() {
        return (
            <View style={viewStyle.view}>
                <TouchableOpacity style={{position: 'absolute', left: 20, top: 25}} onPress={() => this.returnToAlarmView()}>
                    <FlecheRetour></FlecheRetour>
                </TouchableOpacity>
                <View style={viewStyle.textAreaContainer}>
                    <TextInput 
                    style={[viewStyle.textArea, viewStyle.textColor]}
                    multiline
                    placeholderTextColor={'#555555'}
                    placeholder={"Hey ! That's my awesome message I sent to you :)"}
                    autoCorrect={false}
                    keyboardAppearance={"dark"}
                    onChangeText={(newtext) => {
                            this.setState({message: newtext})
                            if(newtext.length > 0) {
                                this.setState({isEmpty: false})
                            } else {
                                this.setState({isEmpty: true})
                            }
                        }
                    }></TextInput>
                </View>
                <View style={viewStyle.taglineContainer}>
                    <Text style={[viewStyle.textColor, viewStyle.tagline, { fontSize: 18 }]}>- François</Text>
                    <Text style={[viewStyle.textColor, viewStyle.tagline, { fontSize: 14 }]}>Nantes, France</Text>
                </View>
                <View style={[viewStyle.buttonContainer, {display: this.state.isEmpty ? 'none' : 'flex'}]}>
                    <CTA text={"Envoyer mon message"} onPress={ () => this.sendMessage()}></CTA>
                </View>
                <View style={[viewStyle.buttonContainer, {display:  this.state.isEmpty ? 'flex' : 'none'}]}>
                    <CTA actif={false} text={"Envoyer mon message"} onPress={ () => {}}></CTA>
                </View>
            </View>
        );
    }
}

const viewStyle = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: styles.primary_color,
        padding: 50,
        display: 'flex',
        position: 'relative',
    },
    textColor: {
        color: styles.secondary_color,
    },
    buttonContainer: {
        marginTop: 50,
        width: "100%",
        flex: 1
    },
    textAreaContainer: {
        flex: 9, 
        paddingRight: 50
    }, 
    textArea: {
        opacity: 1, 
        textAlignVertical: 'top', 
        flex: 1, 
        fontFamily: "Typewriter", 
        fontSize: 20, 
        letterSpacing: 4
    },
    tagline: {
        textAlign: 'right', 
        fontFamily: "Typewriter", 
        letterSpacing: 3
    }, 
    taglineContainer: { 
        flex:1, 
        display: 'flex', 
        justifyContent: 'flex-end'
    }
})