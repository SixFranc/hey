import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import styles from '../styles';
import RingtoneManager from 'react-native-ringtone-manager';
import Sound from 'react-native-sound';
import { CTA } from '../components/CTA';
import { Actions } from 'react-native-router-flux';
import { AppConstantes } from '../app/app.constants'
import FlecheRetour from '../assets/flecheRetour.svg';

/**
 * Vue de choix de sonnerie.
 */
export default class RingList extends Component {
    constructor(props) {
        super(props);
        this.state = {ringList: [], numRing: 0}
        this.sound = undefined;
    }

    /**
     *  Redirection vers la page de programmation du réveil.
     *  Si un son est en train d'être joué, on le stoppe.
     * @param {boolean} withSave Définit si on sauvegarde la sonnerie séléctionnée.
     */
    async returnToAlarmView(withSave) {
        if(this.sound != undefined) {
            this.sound.release();
        }

        if(withSave) {
            await AsyncStorage.setItem('ring', JSON.stringify(this.state.ringList[this.state.numRing]));
        }

        Actions[AppConstantes.ROUTES.alarm]();
    }

    /**
     * Lecture d'une sonnerie. Si une ancienne sonnerie est déjà en train d'être jouée, alors on le stop et on joue la nouvelle.
     * @param {number} numero 
     */
    playSound(numero) {

        if(this.sound != undefined) {
            this.sound.release();
        }

        this.setState({numRing: numero});

        this.sound = new Sound(this.state.ringList[numero].uri, null, (error) => {
                        if (error) {
                            console.log(error)
                        }
                        // play when loaded
                        this.sound.play();

                    });
    }

    /**
     * Chargement de la sonnerie enregistrée. Puis chargement des sonneries du téléphone.
     */
    async componentDidMount() {
        AsyncStorage.getItem('ring').then((data) => {
            if(data) {
                console.log(data);
                let dataParse = JSON.parse(data);
                this.setState({ numRing: dataParse.key});
                console.log(this.state.numRing);
            }
        });

        RingtoneManager.getRingtones((data) => { 
            this.setState({ ringList: data});
        });
    }
    

    render() {
        const {ringList} = this.state;
        return (
            <View style={viewStyle.view}>
                <TouchableOpacity style={{position: 'absolute', left: 20, top: 25}} onPress={() => this.returnToAlarmView()}>
                    <FlecheRetour></FlecheRetour>
                </TouchableOpacity>
                <FlatList style={{flex: 1}}
                    data={ringList}
                    renderItem={({item}) => 
                        <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', heigh: 50, width: '100%',  color: styles.secondary_color, paddingTop: 15, paddingBottom: 15}} onPress={() => this.playSound(item.key)}>
                            <Text style={{color: styles.secondary_color, fontSize: 18, fontFamily: "Roboto Regular"}}>{item.title}</Text>
                            <Image style={{display: this.state.numRing === item.key ? 'flex' : 'none'}} source={require('../assets/tick.png')}></Image>
                        </TouchableOpacity>
                    }
                    keyExtractor={item => item.id}
                ></FlatList>
                <CTA text={"Valider ma sonnerie"} onPress={() => this.returnToAlarmView(true)}></CTA>
            </View>
        );
    }
}

const viewStyle = StyleSheet.create({
    view: {
        flex: 1,
        backgroundColor: styles.primary_color,
        padding: 50,
    },
    textColor: {
        color: styles.secondary_color,
    }
})