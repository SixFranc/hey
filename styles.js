import { StyleSheet } from 'react-native';
const primary_color = '#040410';
const secondary_color = '#FFF';

const styles = StyleSheet.create({ 
    container: {
        flex: 1,
        backgroundColor: primary_color,
        alignItems: 'center',
        justifyContent: 'center',
    },
    texte: {
        color: secondary_color,
    }
});

export default { styles, primary_color, secondary_color };