import React from 'react';
import { Router, Scene } from 'react-native-router-flux';
import AlarmView from '../views/AlarmView';
import RingList from '../views/RingList';
import MessageSending from '../views/MessageSending';
import AlarmRinging from '../views/AlarmRinging';

import { AppConstantes } from './app.constants';

export const AppRouter = () => {
    return (
        <Router>
            <Scene key="root" hideNavBar={true} > 
                <Scene key={AppConstantes.ROUTES.alarm} component={AlarmView} title="Alarm"/>
                <Scene key={AppConstantes.ROUTES.ringList} component={RingList} title="Alarm"/>
                <Scene key={AppConstantes.ROUTES.messageSending} component={MessageSending} title="Message sending"/>
                <Scene key={AppConstantes.ROUTES.alarmRinging} component={AlarmRinging} title="Alarm ringing"/>
            </Scene>
        </Router>
    );
}