"Hey!" est mon projet de fin de master développement Web à l'ECV digital de Nantes.

Application développée sous react-native et testée seulement sur android.
Donc je ne garantis rien sur le bon fonctionnement sur IOS.

C'est une application de réveil permettant reprenant le principe de la bouteille à la mer.
Mis en forme par un système d'envoi et de réception de message. 

Un utilisateur peut envoyer un message par jour, et lorsque son réveil sonne en reçoit un.

L'objectif est d'améliorer le moment du réveil de l'utilisateur en créant une bulle à ce moment à travers la récéption de message.
J'engage l'envoyeur dans une démarche positive et altruiste pour que la qualité et l'impact du message soit au top.


Installation sur android : 
**react-native run-android**

Lancement (avec portable branché en USB) : 
**react-native start**