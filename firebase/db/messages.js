import { db } from '../firebase.js';


/**
 * Sauvegarde du message en BDD
 * @param {*} messageObject 
 */
export async function saveMessage(messageObject) {
    db.collection("messages").get().then(function(querySnapshot) { 
        //Le '' + est indispensable pour que l'objet soit valide pour firebase.
        return db.collection("messages").doc('message' + querySnapshot.size).set({
            message: '' + messageObject.message,
            tagline: '' + messageObject.tagline,
            location: '' + messageObject.location,
        })
    })
}

/**
 * Récupération aléatoire d'un message
 */
export async function getRandomMessage() {
    return db.collection("messages").get().then(async (querySnapshot) => { 
        let randomNumber = Math.trunc(Math.random() * querySnapshot.size);
        let docRef =  db.collection('messages').doc('message' + randomNumber );
        console.log(querySnapshot.size, randomNumber);
        let doc = await docRef.get();
        if (doc.exists) {
            return doc.data();
        } else {
            console.log("No such document!");
        }
    });   
}