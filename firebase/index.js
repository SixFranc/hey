import firebase from './firebase';
import * as messagesDB from './db/messages'

export {
    firebase,
    messagesDB
}