import firebase from 'firebase/app';
import 'firebase/firestore'

//Déclaration de la BDD
const firebaseConfig = {
    apiKey: "AIzaSyAOSdF-DuR5gPc9xmArNrDItnXyC9xkLyA",
    authDomain: "hey-53d1e.firebaseapp.com",
    databaseURL: "https://hey-53d1e.firebaseio.com",
    projectId: "hey-53d1e",
    storageBucket: "hey-53d1e.appspot.com",
    messagingSenderId: "37998916283",
    appId: "1:37998916283:web:3653a26d150945ddce6f9e"
};

// Initialise firebase project with conf
if(!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

const db = firebase.firestore();

export {
    db,
    firebase,
}